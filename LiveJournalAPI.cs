

using System;
using CookComputing.XmlRpc;

namespace Daybook {
	interface ILiveJournal {
		struct ArgBase {
			string username;
			string password;
			string hpassword;
			string ver;
		}

		struct Login : ArgBase {
			string clientversion;
			int getmoods;
			bool getmenus;
			bool getpickws;
			bool getpickwurls;
		}

		struct FriendGroup {
			int id;
			string name;
			int sortorder;
			bool public;
		}

		struct Mood {
			int id;
			string name;
			int parent;
		}

		struct Menu {
			string text;
			string url;
			Menu[] sub;
		}

		struct LoginResponse {
			string fullname;
			string message;
			FriendGroup[] friendgroups;
			string[] usejournals;
			Mood[] moods;
			string[] pickws;
			string[] pickwurls;
			string defaultpicurl;
			bool fastserver;
			string userid;
			Menu[] menus;
		}

		[XmlRpcMethod ("LJ.XMLRPC.login")]
		LoginResponse login (Login args);

		struct Postevent : ArgBase {
			string event;
			string lineendings;
			string subject;
			string security;
			int    allowmask;
			int    year;
			int    day;
			int    hour;
			int    minute;
			MetaData[] props;
			string usejournal;
		}

		struct PosteventResponse {
			string anum;
			string itemid;
		}

		[XmlRpcMethod ("LJ.XMLRPC.postevent")]
		PosteventResponse postevent (Postevent args);

		struct Editevent : Postevent {
			string itemid;
		}

		struct EditeventResponse {
			string itemid;
		}

		[XmlRpcMethod ("LJ.XMLRPC.editevent")]
		EditeventResponse editevent (Editevent args);

		struct FriendgroupSet {
			string name;
			int sort;
			bool public;
		}

		struct Editfriendgroups : ArgBase {
			int groupmasks;
			FriendgroupSet[] set;
			int[] delete;
		}

		[XmlRpcMethod ("LJ.XMLRPC.editfriendgroups")]
		void editevent (Editfriendgroups args);

		struct Editfriends : ArgBase {
			
		}
	}
}
