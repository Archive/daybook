
using System;
using System.Collections;
using System.IO;

using Gtk;

namespace Daybook {
	public class ResourceManager {
		internal string location;
		internal UserToken user;

		public Stream GetResource (string url) {
			Uri loc = new Uri (location);
			switch (loc.Scheme) {
			case "ftp":
				break;
			case "sftp":
				break;
			case "http":
				break;
			case "file":
				break;
			}
			return null;
		}

		public void PutResource (string url, string name, Stream data) {
		}

		public string Location {
			get { return location; }
			set { location = value; }
		}

		public UserToken User {
			get { return user; }
			set { user = value; }
		}
	}

	public class ResourceSelect : Gtk.HBox {
		public ResourceSelect (ArrayList mgrs) : base (false, 4) {
			Gtk.Menu menu = new Gtk.Menu ();

			foreach (ResourceManager mgr1 in mgrs) {
				Gtk.MenuItem backend_item;
				bool show_user = false;

				foreach (ResourceManager mgr2 in mgrs) {
					if (mgr1 != mgr2 && mgr1.Location == mgr2.Location)
						show_user = true;
				}

				if (show_user)
					backend_item = new Gtk.MenuItem (String.Format ("{0} ({1})", 
											mgr1.Location, 
											mgr1.User.Username));
				else
					backend_item = new Gtk.MenuItem (mgr1.Location);
				
				backend_item.Data ["resource_manager"] = mgr1;
				menu.Append (backend_item);
			}

			Gtk.OptionMenu select = new Gtk.OptionMenu ();
			select.Menu = menu;
			this.Add (select);

			Gtk.Button add_btn = new Gtk.Button (Gtk.Stock.Add);
			add_btn.Clicked += new EventHandler (OnAddClicked);
			this.Add (add_btn);
		}

		class ResourceEdit : Gtk.Window {
			public ResourceEdit (ResourceSelect select) {
				
			}
		}

		void OnAddClicked (object sender, EventArgs a) {
			
		}
	}
}
