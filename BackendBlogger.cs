
using System;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

using CookComputing.XmlRpc;
using Blogger = CookComputing.Blogger;
using MetaWeblog = CookComputing.MetaWeblog;

namespace Daybook {
	public class EntryBlogger : Entry {
		internal BlogBlogger blog;
		internal BackendBlogger backend;
		internal EntryGui gui;
		internal bool published;
		internal DateTime postdate;
		internal string subject;
		internal string content;
		internal ArrayList comments = new ArrayList ();
		internal ArrayList trackbacks = new ArrayList ();
		internal string permalink;

		public string userid;
		public string postid;

		public ArrayList Categories = new ArrayList ();

		internal EntryBlogger (BlogBlogger blog, Blogger.Post post) {
			this.blog = blog;
			this.backend = (BackendBlogger) blog.Backend;
			this.published = true;

			this.userid = post.userid;
			this.postid = post.postid;
			this.content = post.content;
			this.postdate = post.dateCreated;
		}

		internal EntryBlogger (BlogBlogger blog) {
			this.blog = blog;
			this.backend = (BackendBlogger) blog.Backend;

			this.Categories = new ArrayList ();
		}

		// Called on deserialization
		public EntryBlogger () {
		}

		public void Post (bool publish) {			
			Console.WriteLine ("Posting entry: \n" +
			                   "Subject: {0}\n" +
					   "Content: {1}\n", 
					   Subject, 
					   Content);

			// serialize categories
			// serialize any Enclosure stuff?
			// set Source from blogger.getUserInfo?

			string postid = null;

			if (!this.published) {
				postid = backend.BloggerAPI.newPost (BackendBlogger.DaybookAppKey,
								     blog.blogid,
								     backend.User.Username,
								     backend.User.Password,
								     this.Content,
								     publish);
			} else {
				backend.BloggerAPI.editPost (BackendBlogger.DaybookAppKey,
							     this.postid,
							     backend.User.Username,
							     backend.User.Password,
							     this.Content,
							     publish);
			}
		}

		public void Delete () {
			Console.WriteLine ("Deleting entry!");

			backend.BloggerAPI.deletePost (BackendBlogger.DaybookAppKey,
						       this.postid,
						       backend.User.Username,
						       backend.User.Password,
						       this.published);
		}

		public string Permalink {
			get { return permalink; }
			set { permalink = value; }
		}

		public DateTime PostDate {
			get { return this.postdate; }
			set { this.postdate = value; }
		}

		public string Subject {
			get { return subject; }
			set { subject = value; }
		}

		public string Content {
			get { return content; }
			set { content = value; }
		}

		public EntryGui DisplayWidget {
			get {
				if (gui == null) {
					gui = new EntryGui (this);

					GladeHelper ui = new GladeHelper ("BackendBlogger.glade", "PostMetas");

					gui.AddMetaElement ("Categories", blog.CategoriesCheck, ui.Unparent ("Categories"));
					gui.AddMetaElement ("Postdate", blog.PostdateCheck, ui.Unparent ("Postdate"));
				}
				return gui;
			}
		}

		[XmlArrayItem ("Comment")]
		public ArrayList Comments {
			get { return new ArrayList (); }
			set { comments = value; }
		}

		[XmlArrayItem ("Trackback")]
		public ArrayList Trackbacks {
			get { return new ArrayList (); }
			set { trackbacks = value; }
		}

		[XmlIgnore]
		public Blog Blog {
			get { return blog; }
			set { blog = (BlogBlogger) value; }
		}
	}

	public class BlogBlogger : Blog {
		internal UserToken user;
		internal BackendBlogger backend;
		internal ArrayList entries = new ArrayList ();
		internal PreferencesHelper preferences;

		// Preferences
		internal Gtk.CheckMenuItem CategoriesCheck;
		internal Gtk.CheckMenuItem PostdateCheck;
		internal Gtk.SeparatorMenuItem Separator1 = new Gtk.SeparatorMenuItem ();

		public string blogid;
		public string url;
		public string blogName;

		public bool Fetched = false;

		Blogger.Category[] Categories;
		MetaWeblog.CategoryInfo[] CategoryInfos;

		internal BlogBlogger (BackendBlogger backend, Blogger.BlogInfo info) : this () {
			this.backend = backend;
			this.blogid = info.blogid;
			this.url = info.url;
			this.blogName = info.blogName;

			this.Preferences = new PreferencesHelper ();

			/*
			try {
				CategoryInfos = backend.MetaWeblogAPI.getCategories (this.blogid,
										     backend.User.Username,
										     backend.User.Password);
				foreach (MetaWeblog.CategoryInfo cat in CategoryInfos) {
				}
			} catch (Exception e) {
				try {
					Categories = backend.BloggerAPI.getCategories (this.blogid,
										       backend.User.Username,
										       backend.User.Password);
					foreach (Blogger.Category cat in Categories) {
					}
				} catch (Exception e2) {
				}
			} finally {
				Console.WriteLine ("No getCategories method call is supported on {0}",
						   backend.Url);
			}
			*/
		}

		public PreferencesHelper Preferences {
			get { return preferences; }
			set {
				preferences = value;
				preferences.Changed += new EventHandler (backend.EmitChanged);

				this.CategoriesCheck = preferences.AddCheckMenuItem ("Post Categories", true);
				this.PostdateCheck = preferences.AddCheckMenuItem ("Post Date", false);
			}
		}

		public BlogBlogger () {
		}

		public bool SupportsComments {
			get { return true; }
		}
		public bool SupportsTrackbacks {
			get { return true; }
		}
		public bool SupportsUploads {
			get { return false; }
		}

		public Stream GetResource (string url) {
			return null;
		}

		public string Name {
			get { return this.blogName; }
			set { this.blogName = value; }
		}

		public string Url {
			get { return this.url; }
			set { this.url = value; }
		}

		public Gdk.Pixbuf Icon {
			get { return new Gdk.Pixbuf (null, "blogger16.png"); }
		}

		public Entry CreateEntry () {
			Entry entry = new EntryBlogger (this);
			entries.Add (entry);
			return entry;
		}

		[XmlArrayItem ("Entry")]
		public ArrayList Entries {
			get {
				if (!Fetched) {
					Blogger.Post[] posts;
					posts = backend.BloggerAPI.getRecentPosts (BackendBlogger.DaybookAppKey,
										   this.blogid,
										   backend.User.Username,
										   backend.User.Password,
										   100);
					foreach (Blogger.Post post in posts) {
						Entry new_entry = new EntryBlogger (this, post);
						entries.Add (new_entry);
					}
					Fetched = true;
				}
				return entries; 
			}
			set { entries = value; }
		}

		public TemplateDesc Template {
			get { return null; }
			set { }
		}

		//
		// Activation, menu merging
		//

		public void Activate (Gui gui) {
			Gtk.Menu view_menu = (Gtk.Menu) gui.ViewMenu.Submenu;

			view_menu.Append (this.Separator1);
			view_menu.Append (this.CategoriesCheck);
			view_menu.Append (this.PostdateCheck);

			view_menu.ShowAll ();
		}

		public void Deactivate (Gui gui) {
			Gtk.Menu view_menu = (Gtk.Menu) gui.ViewMenu.Submenu;

			view_menu.Remove (this.Separator1);
			view_menu.Remove (this.CategoriesCheck);
			view_menu.Remove (this.PostdateCheck);
		}

		[XmlIgnore]
		public Backend Backend {
			get { return backend; }
			set { backend = (BackendBlogger) value; }
		}
	}

	[XmlRoot ("Backend")]
	public class BackendBlogger : Backend {
		internal const string DaybookAppKey = "4A91E68B59AD36BA25AEC7627396A892E89184F1E0";

		internal UserToken user = new UserToken ("orphennui", false);
		internal ArrayList blogs = new ArrayList ();
		internal Blogger.IBlogger BloggerAPI;
		internal MetaWeblog.IMetaWeblog MetaWeblogAPI;
		internal DateTime last_saved;

		public string Address = "blogger.com";

		public string url;
		public string email;
		public string nickname;
		public string lastname;
		public string firstname;

		public BackendBlogger () {
		}

		public Type[] GetSerializationTypes () {
			return new Type [] { typeof (BackendBlogger),
					     typeof (BlogBlogger),
					     typeof (EntryBlogger),
					     typeof (Blogger.Post) };
		}

		public string Name {
			get { return "Blogger.com"; }
		}

		public string Description {
			get { return "Push-button publishing for the people."; }
		}

		public Gdk.Pixbuf Icon {
			get { 
				return new Gdk.Pixbuf (null, "blogger16.png");
			}
		}

		public UserToken User {
			get { return user; }
			set { user = value; }
		}

		internal string Url {
			get { return "http://plant.blogger.com/api/RPC2"; }
		}

		public Gtk.Widget ConfigWidget {
			get {
				GladeHelper ui = new GladeHelper ("BackendBlogger.glade", "Config");
				return ui.Unparent ("BloggerConfig");
			}
		}

		void OnAddressChanged (object sender, EventArgs a) {
			this.Address = ((Gtk.Entry) sender).Text;
		}

		void OnUsernameChanged (object sender, EventArgs a) {
			this.user.Username = ((Gtk.Entry) sender).Text;
		}

		void OnPasswordChanged (object sender, EventArgs a) {
			this.user.Password = ((Gtk.Entry) sender).Text;
		}

		void OnSavePasswordToggled (object sender, EventArgs a) {
			this.user.SavePassword = ((Gtk.CheckButton) sender).Active;
		}

		public Gtk.Widget EditWidget {
			get {
				GladeHelper ui = new GladeHelper ("BackendBlogger.glade", "Edit");
				ui.Autoconnect (this);

				((Gtk.Combo) ui ["Address"]).Entry.Text = Address;

				((Gtk.Entry) ui ["Username"]).Text = user.Username;
				((Gtk.Entry) ui ["Password"]).Text = user.Password;
				((Gtk.CheckButton) ui ["SavePassword"]).Active = user.SavePassword;

				return ui.Unparent ("BloggerEdit");
			}
		}

		public bool CheckSettings () {
			return true;
		}

		[XmlArrayItem ("Blogs")]
		public ArrayList Blogs {
			get { return this.blogs; }
			set { this.blogs = value; }
		}

		void SetupRPC () {
			XmlRpcClientProtocol client;

			client = (XmlRpcClientProtocol) XmlRpcProxyGen.Create (typeof (Blogger.IBlogger));
			client.Url = Url;
			//client.Proxy = config.Proxy;
			this.BloggerAPI = (Blogger.IBlogger) client;

			client = (XmlRpcClientProtocol) XmlRpcProxyGen.Create (typeof (MetaWeblog.IMetaWeblog));
			client.Url = Url;
			//client.Proxy = config.Proxy;
			this.MetaWeblogAPI = (MetaWeblog.IMetaWeblog) client;
		}

		public void Initialize (UserToken user) {
			this.user = user;

			SetupRPC ();

			try {
				Blogger.UserInfo info = BloggerAPI.getUserInfo (BackendBlogger.DaybookAppKey,
										user.Username,
										user.Password);
				this.url = info.url;
				this.email = info.email;
				this.nickname = info.nickname;
				this.lastname = info.lastname;
				this.firstname = info.firstname;
			} catch (Exception e) {
			}

			try {
				Blogger.BlogInfo[] bloginfos = BloggerAPI.getUsersBlogs (BackendBlogger.DaybookAppKey,
											 user.Username,
											 user.Password);
				foreach (Blogger.BlogInfo bloginfo in bloginfos) {
					Blog new_blog = new BlogBlogger (this, bloginfo);
					blogs.Add (new_blog);
				}
			} catch (Exception e) {
			}
		}

		public void Refresh (UserToken user) {
		}

		public void Shutdown () {
		}

		public DateTime LastSaved {
			get { return last_saved; }
			set { last_saved = value; }
		}

		public event EventHandler Changed;

		internal void EmitChanged (object sender, EventArgs args) {
			if (Changed != null)
				Changed (this, args);
		}
	}
}
