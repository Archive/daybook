
using System;
using System.IO;
using System.Collections;
using System.Reflection;
		using System.Runtime.InteropServices;

using Gtk;
using GtkSharp;

namespace Daybook {
	public class Foregrounder : Gtk.EventBox {
		public Foregrounder (Gtk.Widget widget) : base () {
			this.Add (widget);
		}
	}

	public class Backgrounder : Gtk.EventBox {
		public Backgrounder (Gtk.Container parent) : base () {
			this.ModifyBg (StateType.Normal, this.Style.Backgrounds[3]);
			this.Add (parent);
		}
	}

	public class SimpleExpander : Gtk.Table {
		Gtk.Button button;
		Gtk.Widget label;
		Gtk.Widget collapsed_content;
		Gtk.Widget content;

		bool expanded = false;
		bool button_down = false;

		public SimpleExpander () : base (2, 2, false) {
			button = new Gtk.Button ();
			button.Add (new Gtk.Arrow (Gtk.ArrowType.Right, 
						   Gtk.ShadowType.In));
			button.Clicked += new EventHandler (ExpandClicked);
			button.Relief = Gtk.ReliefStyle.None;
			button.CanFocus = false;

			Attach (button, 
				0, 1, 0, 1, 
				0, 
				0,
				0, 0);
		}

		public SimpleExpander (string label) : this () {
			this.Label = new Gtk.Label (label);
		}

		public bool Expanded {
			get  { return expanded; }
			set  {
				if (expanded == value)
					return;

				button.Remove (button.Child);

				if (value) {
					button.Add (new Gtk.Arrow (Gtk.ArrowType.Down, 
								   Gtk.ShadowType.In));
					if (content != null) {
						content.ShowAll ();
						Attach (content, 1, 2, 1, 2);
					}
				} else {
					button.Add (new Gtk.Arrow (Gtk.ArrowType.Right, 
								   Gtk.ShadowType.In));
					if (content != null)
						Remove (content);
				}

				button.ShowAll ();

				expanded = value;
			}
		}

		public Gtk.Widget Label {
			get { return label; }
			set { 
				if (label != null)
					Remove (label);

				value.ButtonPressEvent += new GtkSharp.ButtonPressEventHandler (ButtonPress);
				value.ButtonReleaseEvent += new GtkSharp.ButtonReleaseEventHandler (ButtonRelease);

				Attach (value, 1, 2, 0, 1);
				label = value;
			}
		}

		void ButtonPress (object sender, GtkSharp.ButtonPressEventArgs args) {
			if (args.Event.button == 1) {
				button_down = true;
				args.RetVal = true;
			} 
			args.RetVal = false;
		}

		void ButtonRelease (object sender, GtkSharp.ButtonReleaseEventArgs args) {
			if (args.Event.button == 1 && button_down) {
				ExpandClicked (sender, args);
				button_down = false;
				args.RetVal = true;
			} 
			args.RetVal = false;
		}

		public Gtk.Widget Content {
			get { return this.content; }
			set { 
				if (content != null)
					Remove (content);

				if (Expanded) {
					value.ShowAll ();
					Attach (value, 1, 2, 1, 2);
				}

				content = value; 
			}
		}

		void ExpandClicked (object sender, EventArgs args) {
			Expanded = !Expanded;
		}
	}

	public abstract class Editor : Connectable {
		Gtk.Widget widget;
		Entry entry;

		public virtual void Connect (GladeHelper xml) {
			xml.ConnectWidgetTree (this);
		}

		public virtual void Disconnect (GladeHelper xml) {
			xml.DisconnectWidgetTree (this);
		}

		public Gtk.Widget Widget {
			get { return widget; }
			set { widget = value; }
		}

		public Entry Entry {
			get { return entry; }
			set { entry = value; }
		}		
	}

	public class GtkHTMLEditor : Editor {
		Gtk.HTMLEditorAPI api;
		Gtk.HTML html;

		public GtkHTMLEditor (Entry entry) {
			html = new Gtk.HTML ();
			html.LinkClicked += new GtkSharp.LinkClickedHandler (LinkClicked);
			html.UrlRequested += new GtkSharp.UrlRequestedHandler (UrlRequested);
			html.OnUrl += new GtkSharp.OnUrlHandler (OnUrlMouseOver);
			html.SizeChanged += new EventHandler (SizeChanged);
			html.ObjectRequested += new GtkSharp.ObjectRequestedHandler (ObjectRequested);

			Gtk.HTMLStream html_stream = html.Begin ();

			if (entry.Content != null)
				html_stream.Write (entry.Content);

			html.End (html_stream, Gtk.HTMLStreamStatus.Ok);

			html.Editable = true;
			html.InlineSpelling = true;
			html.MagicLinks = true;
			html.MagicSmileys = true;
			html.Animate = true;

			api = new Gtk.HTMLEditorAPI ();
			api.CheckWord = new GtkSharp.HTMLCheckWordHandler (HtmlCheckWord);
			api.SuggestionRequest = new GtkSharp.HTMLSuggestionRequestHandler (HtmlSuggestionRequest);
			api.AddToSession = new GtkSharp.HTMLAddToSessionHandler (HtmlAddToSession);
			api.AddToPersonal = new GtkSharp.HTMLAddToPersonalHandler (HtmlAddToPersonal);
			api.Command = new GtkSharp.HTMLCommandHandler (HtmlCommand);
			api.Event = new GtkSharp.HTMLEventHandler (HtmlEvent);
			api.CreateInputLine = new GtkSharp.HTMLCreateInputLineHandler (HtmlCreateInputLine);
			api.SetLanguage = new GtkSharp.HTMLSetLanguageHandler (HtmlSetLanguage);

			html.SetEditorAPI (api);

			this.Entry = entry;
			this.Widget = html;
		}


		bool HtmlCheckWord (Gtk.HTML html, string word) {
			return false;
		}

		void HtmlSuggestionRequest (Gtk.HTML html) {
		}

		void HtmlAddToSession (Gtk.HTML html, string word) {
		}

		void HtmlAddToPersonal (Gtk.HTML html, string word, string language) {
		}

		bool HtmlCommand (Gtk.HTML ptr, Gtk.HTMLCommandType type) {
			Console.WriteLine ("HtmlCommand called: {0}", type);
			return false;
		}

		GLib.Value HtmlEvent (Gtk.HTML ptr, Gtk.HTMLEditorEventType event_type, GLib.Value args) {
			Console.WriteLine ("HtmlEvent called: {0}, {1}", event_type, args);
			return null;
		}

		Gtk.Widget HtmlCreateInputLine (Gtk.HTML html) {
			Console.WriteLine ("HtmlCreateInputLine called");
			return null;
		}

		void HtmlSetLanguage (Gtk.HTML ptr, string language) {
			Console.WriteLine ("HtmlSetLanguage called: {0}, {1}", ptr, language);
		}
		
		void UrlRequested (object sender, GtkSharp.UrlRequestedArgs args) {
			Stream s;

			s = File.OpenRead ("/home/orph/abby egg.jpg");

			/*
			s = entry.Blog.GetResource (args.Url);
			if (s == null) {
				Assembly asm = Assembly.GetCallingAssembly ();
				s = asm.GetManifestResourceStream ("daybook.png");
			}
			*/

			int n;
			byte [] buffer = new byte[8196];
			while ((n = s.Read (buffer, 0, buffer.Length)) != 0) {
				args.Handle.Write (buffer, n);
			}
			args.Handle.Close (HTMLStreamStatus.Ok);
		}

		void ObjectRequested (object sender, GtkSharp.ObjectRequestedArgs args) {
		}

		void LinkClicked (object sender, GtkSharp.LinkClickedArgs args) {
		}

		void OnUrlMouseOver (object sender, GtkSharp.OnUrlArgs args) {
		}

		void SizeChanged (object sender, EventArgs args) {
			uint x, y;
			this.html.GetSize (out x, out y);
			Console.WriteLine ("HTML Size Changed, Getsize ({0}, {1}), Request ({2}, {3}) !!", 
					   x, y,
					   this.html.WidthRequest,
					   this.html.HeightRequest);

			this.html.HeightRequest = y > 100 ? (int) y : 100;
			this.html.QueueResize ();

			this.html.Vadjustment.Value = 0;
			this.html.Vadjustment.ChangeValue ();
		}

		//
		// Glade callbacks
		//

		// Menu items

		void OnUndoActivate (object sender, EventArgs a) {
			html.Undo ();
		}
		void OnRedoActivate (object sender, EventArgs a) {
			html.Redo ();
		}

		void OnCutActivate (object sender, EventArgs a) {
			html.Cut ();
		}
		void OnCopyActivate (object sender, EventArgs a) {
			html.Copy ();
		}
		void OnPasteActivate (object sender, EventArgs a) {
			html.Paste (false);
		}
		void OnPasteAsQuoteActivate (object sender, EventArgs a) {
			html.Paste (true);
		}
		void OnClearActivate (object sender, EventArgs a) {
			// FIXME
		}

		// Edit Bar buttons

		void OnFontSizeOptionChanged (object sender, EventArgs a) {
			// FIXME
		}

		void OnParagraphOptionChanged (object sender, EventArgs a) {
			// FIXME
		}

		void OnBoldToggled (object sender, EventArgs a) {
			// FIXME
		}
		void OnItalicsToggled (object sender, EventArgs a) {
			// FIXME
		}
		void OnUnderlineToggled (object sender, EventArgs a) {
			// FIXME
		}
		void OnStrikeoutToggled (object sender, EventArgs a) {
			// FIXME
		}

		void OnLeftAlignToggled (object sender, EventArgs a) {
			html.ParagraphAlignment = HTMLParagraphAlignment.Left;
		}
		void OnCenterAlignToggled (object sender, EventArgs a) {
			html.ParagraphAlignment = HTMLParagraphAlignment.Center;
		}
		void OnRightAlignToggled (object sender, EventArgs a) {
			html.ParagraphAlignment = HTMLParagraphAlignment.Right;
		}

		void OnIndentActivate (object sender, EventArgs a) {
			// FIXME: GtkSharp bug
			//html.IndentPushLevel ();
		}

		void OnUnindentActivate (object sender, EventArgs a) {
			html.IndentPopLevel ();
		}

		void OnColorToggled (object sender, EventArgs a) {
			// FIXME
		}

		void OnAddTableToggled (object sender, EventArgs a) {
			// FIXME
		}
	}

	public class TextEditor : Editor {
		Gtk.TextView text;

		public TextEditor (Entry entry) {
			Gtk.TextTagTable tag_table = new Gtk.TextTagTable ();

			Gtk.TextBuffer buffer = new Gtk.TextBuffer (tag_table);
			if (entry.Content != null)
				buffer.Text = entry.Content;
			buffer.Changed += new EventHandler (TextBufferChanged);

			this.text = new Gtk.TextView (buffer);

			this.Entry = entry;
			this.Widget = text;
		}

		void TextBufferChanged (object sender, EventArgs args) {
			Gtk.TextBuffer buffer = (Gtk.TextBuffer) sender;
			this.Entry.Content = buffer.Text;
		}

		//
		// Glade callbacks
		//

		// Menu items

		void OnUndoActivate (object sender, EventArgs a) {
		}
		void OnRedoActivate (object sender, EventArgs a) {
		}

		void OnCutActivate (object sender, EventArgs a) {
		}
		void OnCopyActivate (object sender, EventArgs a) {
		}
		void OnPasteActivate (object sender, EventArgs a) {
		}
		void OnPasteAsQuoteActivate (object sender, EventArgs a) {
		}
		void OnClearActivate (object sender, EventArgs a) {
		}

		// Edit Bar buttons

		void OnFontSizeOptionChanged (object sender, EventArgs a) {
		}

		void OnParagraphOptionChanged (object sender, EventArgs a) {
		}

		void OnBoldToggled (object sender, EventArgs a) {
		}
		void OnItalicsToggled (object sender, EventArgs a) {
		}
		void OnUnderlineToggled (object sender, EventArgs a) {
		}
		void OnStrikeoutToggled (object sender, EventArgs a) {
		}

		void OnLeftAlignToggled (object sender, EventArgs a) {
		}
		void OnCenterAlignToggled (object sender, EventArgs a) {
		}
		void OnRightAlignToggled (object sender, EventArgs a) {
		}

		void OnIndentActivate (object sender, EventArgs a) {
		}
		void OnUnindentActivate (object sender, EventArgs a) {
		}

		void OnColorToggled (object sender, EventArgs a) {
		}

		void OnAddTableToggled (object sender, EventArgs a) {
		}
	}

	public class MozillaViewer : Editor {
		public MozillaViewer (Entry entry) {
			this.Entry = entry;
			this.Widget = new Gtk.Label ("Mozilla Preview not yet implemented");
		}

		public override void Connect (GladeHelper xml) {
			xml ["EditBar"].Hide ();
			xml ["FormatMenu"].Hide ();
			xml ["InsertMenu"].Hide ();

			xml ["UndoMenuItem"].Sensitive = 
				xml ["RedoMenuItem"].Sensitive = 
				xml ["CutMenuItem"].Sensitive = 
				xml ["CopyMenuItem"].Sensitive = 
				xml ["PasteMenuItem"].Sensitive = 
				xml ["PasteAsQuoteMenuItem"].Sensitive = 
				xml ["ClearMenuItem"].Sensitive = 
				xml ["ReplaceMenuItem"].Sensitive = false;
		}

		public override void Disconnect (GladeHelper xml) {
			xml ["EditBar"].Show ();
			xml ["FormatMenu"].Show ();
			xml ["InsertMenu"].Show ();

			xml ["UndoMenuItem"].Sensitive = 
				xml ["RedoMenuItem"].Sensitive = 
				xml ["CutMenuItem"].Sensitive = 
				xml ["CopyMenuItem"].Sensitive = 
				xml ["PasteMenuItem"].Sensitive = 
				xml ["PasteAsQuoteMenuItem"].Sensitive = 
				xml ["ClearMenuItem"].Sensitive = 
				xml ["ReplaceMenuItem"].Sensitive = true;
		}
	}

	public class EntryGui : SimpleExpander, Connectable {
		Entry entry;
		GladeHelper ui;
		Gtk.VBox content_vbox;
		MetaGui meta_gui;
		Editor editor;
		SimpleExpander comments;
		SimpleExpander trackbacks;

		bool editor_focused;

		public EntryGui (Entry entry) : base () {
			this.entry = entry;
			this.meta_gui = new MetaGui ();

			this.Label = SetupLabel ();
			this.Content = SetupContent ();
		}

		string FormatDate (DateTime date) {
			if (date.Year == DateTime.Now.Year)
				return date.ToString ("dddd, MMMM dd, h:mm tt", null);
			else
				return date.ToString ("f", null); 
		}

		Gtk.Widget SetupLabel () {
			Gdk.Color green_color = new Gdk.Color ();
			Gdk.Color.Parse ("light green", ref green_color);

			Gtk.EventBox event_box = new Gtk.EventBox ();
			event_box.ModifyBg (Gtk.StateType.Normal, green_color);
			event_box.BorderWidth = 2;

			string label_text;
			if (entry.Subject == null)
				label_text = String.Format ("<big><b>{0}</b></big>", 
							    FormatDate (entry.PostDate));
			else if (entry.Subject == String.Empty)
				label_text = String.Format ("<big>(No Subject)</big> ({1})",
							    FormatDate (entry.PostDate));
			else 
				label_text = String.Format ("<big><b>{0}</b></big> ({1})",
							    entry.Subject,
							    FormatDate (entry.PostDate));

			Gtk.Label label = new Gtk.Label (label_text);
			label.UseMarkup = true;
			label.LineWrap = true;
			event_box.Add (label);

			return new Backgrounder (event_box);
		}

		Gtk.Widget SetupContent () {
			content_vbox = new Gtk.VBox (false, 2);
			content_vbox.BorderWidth = 2;
			content_vbox.FocusChildSet += new GtkSharp.FocusChildSetHandler (FocusChildSetHandler);

			content_vbox.PackStart (new Backgrounder (this.meta_gui), false, false, 0);

			// Add gtkhtml editing widget
			// FIXME: This should remember the last setting
			OnVisualEditingActivate (null, null);

			if (entry.Blog.SupportsComments) {
				this.comments = new CommentsGui (entry);
				content_vbox.PackEnd (new Foregrounder (this.comments), false, false, 0);
			}

			if (entry.Blog.SupportsTrackbacks) {
				this.trackbacks = new TrackbackGui (entry);
				content_vbox.PackEnd (new Foregrounder (this.trackbacks), false, false, 0);
			}

			return new Backgrounder (content_vbox);
		}

		public void AddMetaElement (string name,
					    Gtk.CheckMenuItem check,
					    Gtk.Widget content) {
			meta_gui.AddMetaElement (name,
						 check,
						 content);
		}

		public Entry Entry {
			get { return entry; }
		}

		public Editor Editor {
			get { return editor; }
			set { 
				if (this.editor != null)
					this.content_vbox.Remove (this.editor.Widget);

				value.Widget.ShowAll ();
				this.content_vbox.PackStart (value.Widget, false, false, 0);

				this.editor = value; 
			}
		}

		void OnVisualEditingActivate (object sender, EventArgs a) {
			this.Editor = new GtkHTMLEditor (this.Entry);
		}

		void OnHTMLEditingActivate (object sender, EventArgs a) {
			this.Editor = new TextEditor (this.Entry);
		}

		void OnPreviewActivate (object sender, EventArgs a) {
			this.Editor = new MozillaViewer (this.Entry);
		}

		[Glade.Widget] public Gtk.VBox     ContentVBox;
		[Glade.Widget] public Gtk.Toolbar  Editbar;
		[Glade.Widget] public Gtk.MenuItem FormatMenu;
		[Glade.Widget] public Gtk.MenuItem InsertMenu;

		void FocusChildSetHandler (object sender, GtkSharp.FocusChildSetArgs args) {
			if (args.Widget == Editor.Widget) {
				if (!editor_focused) {
					Editor.Connect (this.ui);

					Editbar.Show ();
					Editbar.Sensitive = true;

					FormatMenu.Show ();
					InsertMenu.Show ();

					ui ["UndoMenuItem"].Sensitive = true;
					ui ["RedoMenuItem"].Sensitive = true;
					ui ["PasteAsQuoteMenuItem"].Sensitive = true;

					editor_focused = true;
				}
			} else if (editor_focused) {
				if (args.Widget == null) {
					Editor.Disconnect (this.ui);

					Editbar.Sensitive = false;

					//Editbar.Hide ();
					FormatMenu.Hide ();
					InsertMenu.Hide ();

					ui ["UndoMenuItem"].Sensitive = false;
					ui ["RedoMenuItem"].Sensitive = false;
					ui ["PasteAsQuoteMenuItem"].Sensitive = false;

					editor_focused = false;
				}
			}

			Console.WriteLine ("FocusChildSetHandler Widget = '{0}'", args.Widget);
		}

		void FocusChildSetHandlerVBox (object sender, GtkSharp.FocusChildSetArgs args) {
			Console.WriteLine ("ContentVBox Focus Widget = '{0}'", args.Widget);
		}

		public void Connect (GladeHelper ui) {
			ui.ConnectWidgetTree (this);
			this.ui = ui;
		}

		public void Disconnect (GladeHelper ui) {
			content_vbox.FocusChildSet -= new GtkSharp.FocusChildSetHandler (FocusChildSetHandler);
			ui.DisconnectWidgetTree (this);
			this.ui = null;
		}
	}

}
