
using System;
using Gtk;
using GtkSharp;
using Gnome;

using System.Xml.Serialization;

namespace Daybook {
	public class UserToken {
		[Glade.Widget] Gtk.Dialog Login;
		[Glade.Widget] Gtk.Label ReasonLabel;
		[Glade.Widget] Gtk.Entry UsernameEntry;
		[Glade.Widget] Gtk.Entry PasswordEntry;
		[Glade.Widget] Gtk.CheckButton SavePasswordCheck;
		[Glade.Widget] Gtk.CheckButton ConnectProxyCheck;

		string username;
		string password;
		string address;
		bool save_password;

		public string Username {
			get { return username; }
			set {
				username = value; 
				UsernameEntry.Text = value;
			}
		}

		[XmlIgnore]
		public string Password {
			get { 
				if (!save_password)
					AskPassword ();
				return password; 
			}
			set { 
				password = value; 
				if (save_password)
					PasswordEntry.Text = value;
			}
		}

		public bool SavePassword {
			get { return save_password; }
			set { save_password = value; }
		}

		public string SavedPassword {
			get {
				if (save_password)
					return password;
				else
					return null;
			}
			set { Password = value; }
		}

		public bool AskPassword () {
			Gdk.Threads.Enter ();
			Gtk.ResponseType response = (Gtk.ResponseType) Login.Run ();
			Login.Hide ();
			Gdk.Threads.Leave ();

			if (response == Gtk.ResponseType.Ok) {
				if (SavePasswordCheck.Active)
					password = PasswordEntry.Text;

				return true;
			} else {
				return false;
			}
		}

		public bool AskPassword (string reason) {
			string last = ReasonLabel.Text;
			ReasonLabel.Text = reason;

			bool ret = AskPassword ();

			ReasonLabel.Text = last;

			return ret;
		}

		void OnConnectProxyToggled (object sender, EventArgs a) {
			// FIXME: show proxy host/port, username, password
			//        widgets (use separate dialog?)
		}

		public string Reason {
			set { ReasonLabel.Text = value; }
		}

		public string Address {
			set { 
				ReasonLabel.Text = String.Format ("Please provide a password for\n" +
								  "<b><big>{0}</big></b>\n",
								  value);
				ReasonLabel.UseMarkup = true;
				address = value;
			}
			get { return address; }
		}

		public UserToken () {
			GladeHelper ui = new GladeHelper ("Daybook.glade", "Login");
			ui.Autoconnect (this);
		}

		public UserToken (string username, bool save_password) : base () {
			this.username = username;
			this.save_password = save_password;
		}
	}

	public class Daybook {
		BackendManager backend_manager;

		public static void Main (string [] args) {
			//Application.Init ();
			Program daybook = new Program ("Daybook", 
						       "0.0", 
						       Modules.UI, 
						       args);

			Daybook d = new Daybook ();

			Gdk.Threads.Init ();

			daybook.Run ();
			//Application.Run ();
		}

		public Daybook () {
			this.backend_manager = new BackendManager ();

			StartMainWindow ();

			if (backend_manager.Blogs.Count == 0) {
				StartAddProfile ();
			}
		}

		void StartAddProfile () {
			Gtk.Window AddProfile = new Gtk.Window ("Configure Your Weblog");
		}

		void StartMainWindow () {
			//need to load/store the active blog
			Gui gui = new Gui (backend_manager, null);
			gui.MainWindow.DeleteEvent += new DeleteEventHandler (DeleteMainWindow);
		}

		void DeleteMainWindow (object o, DeleteEventArgs args) {
			Application.Quit ();
			args.RetVal = true;
		}
	}
}
