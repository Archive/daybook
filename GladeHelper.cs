
using System;
using System.Reflection;
using System.Collections;

namespace Daybook {
	public class GladeHelper : Glade.XML {
		public GladeHelper (string resource_name, string root) 
			: base (Assembly.GetCallingAssembly (), resource_name, root, null)
		{
		}

		public Gtk.Widget Unparent (string name) {
			Gtk.Widget widget = GetWidget (name);
			if (widget != null) {
				Gtk.Container parent = (Gtk.Container) widget.Parent;
				if (parent != null) 
					parent.Remove (widget);
			}
			return widget;
		}

		[System.Runtime.InteropServices.DllImport("libglade-2.0-0.dll")]
		static extern void glade_xml_signal_autoconnect_full (IntPtr raw, 
								      RawXMLConnectFunc func,
								      IntPtr user_data);

		delegate void RawXMLConnectFunc (string handler_name, 
						 IntPtr objekt,
						 string signal_name, 
						 string signal_data,
						 IntPtr connect_object,
						 int after, 
						 IntPtr user_data);

		public void ConnectWidgetTree (object handler) {
			this.BindFields (handler);

			Connector con = new Connector (handler, true);

			glade_xml_signal_autoconnect_full (this.Handle,
							   new RawXMLConnectFunc (con.Connect), 
							   IntPtr.Zero);
		}

		public void DisconnectWidgetTree (object handler) {
			Connector con = new Connector (handler, false);

			glade_xml_signal_autoconnect_full (this.Handle,
							   new RawXMLConnectFunc (con.Connect), 
							   IntPtr.Zero);
		}

		class Connector {
			object handler_object;
			Hashtable handler_methods;
			bool connect;

			const BindingFlags SearchFlags = (BindingFlags.Instance | 
							  BindingFlags.Static | 
							  BindingFlags.Public | 
							  BindingFlags.NonPublic);
			static MemberFilter SignalFilter = new MemberFilter (FilterSignalName);

			internal Connector (object handler_object, bool connect) {
				this.connect = connect;
				this.handler_object = handler_object;
				this.handler_methods = new Hashtable ();

				foreach (MethodInfo meth in handler_object.GetType ().GetMethods (SearchFlags)) {
					this.handler_methods [meth.Name] = true;
				}
			}

			internal void Connect (string handler_name, 
					       IntPtr objekt_ptr, 
					       string signal_name, 
					       string signal_data, 
					       IntPtr connect_object_ptr, 
					       int after,
					       IntPtr user_data) {
				/* Chack that a handler named handler_name exists on the target */
				if (this.handler_methods [handler_name] == null)
					return;

				/* get the object */
				GLib.Object objekt = GLib.Object.GetObject (objekt_ptr, false);

				/* if an connect_object_ptr is provided, use that as handler */
				object connect_object = 
					connect_object_ptr == IntPtr.Zero ? 
						handler_object : 
						GLib.Object.GetObject (connect_object_ptr, false);

				/* search for the event to connect */
				System.Reflection.MemberInfo[] evnts = 
					objekt.GetType ().FindMembers (MemberTypes.Event, 
								       SearchFlags, 
								       SignalFilter, 
								       signal_name);
				foreach (System.Reflection.EventInfo ei in evnts) {
					MethodInfo meth;

					if (this.connect) 
						meth = ei.GetAddMethod ();
					else
						meth = ei.GetRemoveMethod ();

					ParameterInfo[] methpi = meth.GetParameters ();
					if (methpi.Length == 1) {
						/* this should be always true, unless there's something broken */
						Type delegate_type = methpi[0].ParameterType;

						try {
							/* look for an instance method */
							Delegate d = Delegate.CreateDelegate (delegate_type, 
											      connect_object, 
											      handler_name);
							meth.Invoke (objekt, new object[] { d } );
						} catch (ArgumentException e) {
							Console.WriteLine ("Unable to {0} method {1} to {2}", 
									   this.connect ? "connect" : "disconnect",
									   handler_name,
									   handler_object.GetType().FullName);
							/* ignore if there is not such instance method */
						}
					}
				}
			}

			/* matches events to GLib signal names */
			static bool FilterSignalName (MemberInfo m, object filterCriteria) 
			{
				string signame = (filterCriteria as string);
				object[] attrs = m.GetCustomAttributes (typeof (GLib.SignalAttribute), true);
				if (attrs.Length > 0) {
					foreach (GLib.SignalAttribute a in attrs) {
						if (signame == a.CName) {
							return true;
						}
					}
					return false;
				} else {
					/* this tries to match the names when no attibutes are present.
					   It is only a fallback. */
					signame = signame.ToLower ().Replace ("_", "");
					string evname = m.Name.ToLower ();
					return signame == evname;
				}
			}
			
		}
	}
}
