
using System;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using System.Reflection;

namespace Daybook {
	public class EntryTest : Entry {
		internal EntryGui gui;
		internal DateTime postdate = DateTime.Now;
		internal string subject;
		internal string content;
		internal BlogTest blog;
		internal ArrayList comments = new ArrayList ();
		internal ArrayList trackbacks = new ArrayList ();
		internal string permalink = "http://mybutt.com";

		internal EntryTest (BlogTest blog, string subject, string content) {
			this.blog = blog;
			this.subject = subject;
			this.content = content;

			Console.WriteLine ("ENTRYTEST INIT FROM USER CODE");
		}

		public EntryTest () {
			Console.WriteLine ("EntryTest .cons");
		}

		public void Post (bool publish) {
			Console.WriteLine ("Posting entry: \n" +
			                   "Subject: {0}\n" +
					   "Content: {1}\n", 
					   Subject, 
					   Content);
		}

		public void Delete () {
			Console.WriteLine ("Deleting entry!");
		}

		public string Permalink {
			get { return permalink; }
			set { permalink = value; }
		}

		public DateTime PostDate {
			get { return postdate; }
			set { postdate = value; }
		}

		public string Subject {
			get { return subject; }
			set { subject = value; }
		}

		public string Content {
			get { return content; }
			set { content = value; }
		}

		public EntryGui DisplayWidget {
			get {
				if (gui == null) {
					gui = new EntryGui (this);

					GladeHelper ui = new GladeHelper ("Backend.glade", "PostMetas");

					gui.AddMetaElement ("Music", blog.MusicCheck, ui.Unparent ("Music"));
					gui.AddMetaElement ("Mood", blog.MoodCheck, ui.Unparent ("Mood"));
					gui.AddMetaElement ("Categories", blog.CategoriesCheck, ui.Unparent ("Categories"));
					gui.AddMetaElement ("Security", blog.SecurityCheck, ui.Unparent ("Security"));
					gui.AddMetaElement ("Postdate", blog.PostdateCheck, ui.Unparent ("Postdate"));

					return gui;
				}
				return gui;
			}
		}

		[XmlArrayItem ("Comment")]
		public ArrayList Comments {
			get { return comments; }
			set { comments = value; }
		}

		[XmlArrayItem ("TrackBack")]
		public ArrayList Trackbacks {
			get { return comments; }
			set { trackbacks = value; }
		}

		[XmlIgnore]
		public Blog Blog {
			get { return blog; }
			set { blog = (BlogTest) value; }
		}
	}

	public class BlogTest : Blog {
		internal BackendTest backend;
		internal ArrayList entries = new ArrayList ();
		internal string name;
		internal string url;
		internal PreferencesHelper preferences;

		// Preferences
		internal Gtk.CheckMenuItem MusicCheck;
		internal Gtk.CheckMenuItem MoodCheck;
		internal Gtk.CheckMenuItem CategoriesCheck;
		internal Gtk.CheckMenuItem SecurityCheck;
		internal Gtk.CheckMenuItem PostdateCheck;
		internal Gtk.RadioMenuItem GuiEditRadio;
		internal Gtk.RadioMenuItem TextEditRadio;
		internal Gtk.SeparatorMenuItem Separator1 = new Gtk.SeparatorMenuItem ();

		internal BlogTest (BackendTest backend) : this () {
			this.backend = backend;
			this.name = Backend.User.Username;

			this.entries.Add (new EntryTest (this, 
							 "First Post",
							 "First post!!!!<br><br><img src=\"file:///home/orph/abby egg.jpg\">"));
			this.entries.Add (new EntryTest (this, "Second Post", "Second test post!!!!"));

			this.Preferences = new PreferencesHelper ();
		}

		public PreferencesHelper Preferences {
			get { return preferences; }
			set {
				preferences = value;
				preferences.Changed += new EventHandler (backend.EmitChanged);

				this.MusicCheck = preferences.AddCheckMenuItem ("Current Music", true);
				this.MoodCheck = preferences.AddCheckMenuItem ("Current Mood", true);
				this.CategoriesCheck = preferences.AddCheckMenuItem ("Post Categories", true);
				this.SecurityCheck = preferences.AddCheckMenuItem ("Post Security", true);
				this.PostdateCheck = preferences.AddCheckMenuItem ("Postdate", true);
			}
		}

		public BlogTest () {
			Console.WriteLine ("BlogTest .cons");
		}

		public bool SupportsComments {
			get { return true; }
		}
		public bool SupportsTrackbacks {
			get { return true; }
		}
		public bool SupportsUploads {
			get { return true; }
		}

		public Stream GetResource (string url) {
			return null;
		}

		public string Name {
			get { return name; }
			set { name = value; }
		}

		public string Url {
			get { return url; }
			set { url = value; }
		}

		public Gdk.Pixbuf Icon {
			get { return null; }
		}

		public Entry CreateEntry () {
			Entry entry = new EntryTest (this, null, null);
			entries.Add (entry);
			return entry;
		}

		[XmlArrayItem ("Entry")]
		public ArrayList Entries {
			get { return entries; }
			set { entries = value; }
		}

		public TemplateDesc Template {
			get { return null; }
			set { }
		}

		//
		// Activation, menu merging
		//

		public void Activate (Gui gui) {
			Gtk.Menu view_menu = (Gtk.Menu) gui.ViewMenu.Submenu;

			view_menu.Append (this.Separator1);
			view_menu.Append (this.MusicCheck);
			view_menu.Append (this.MoodCheck);
			view_menu.Append (this.CategoriesCheck);
			view_menu.Append (this.SecurityCheck);
			view_menu.Append (this.PostdateCheck);

			view_menu.ShowAll ();
		}

		public void Deactivate (Gui gui) {
			Gtk.Menu view_menu = (Gtk.Menu) gui.ViewMenu.Submenu;

			view_menu.Remove (this.Separator1);
			view_menu.Remove (this.MusicCheck);
			view_menu.Remove (this.MoodCheck);
			view_menu.Remove (this.CategoriesCheck);
			view_menu.Remove (this.SecurityCheck);
			view_menu.Remove (this.PostdateCheck);
		}

		[XmlIgnore]
		public Backend Backend {
			get { return backend; }
			set { backend = (BackendTest) value; }
		}
	}

	[XmlRoot ("Backend")]
	public class BackendTest : Backend {
		internal UserToken user = new UserToken ("orphennui", false);
		internal ArrayList blogs = new ArrayList ();
		internal DateTime last_saved;

		public BackendTest () {
			Console.WriteLine ("BackendTest .cons");
		}

		public Type[] GetSerializationTypes () {
			return new Type [] { typeof (BackendTest),
					     typeof (BlogTest),
					     typeof (EntryTest) };
		}

		public string Name {
			get { return "Test Backend"; }
		}

		public string Description {
			get { return "Create a new test backend profile"; }
		}

		public Gdk.Pixbuf Icon {
			get { return null; }
		}

		public UserToken User {
			get { return user; }
			set { user = value; }
		}

		public Gtk.Widget ConfigWidget {
			get {
				GladeHelper ui = new GladeHelper ("Backend.glade", "PostMetas");
				return ui.Unparent ("TestConfig");
			}
		}

		public Gtk.Widget EditWidget {
			get {
				GladeHelper ui = new GladeHelper ("Backend.glade", "TestEditTopLevel");
				return ui.Unparent ("TestEdit");
			}
		}

		public bool CheckSettings () {
			return true;
		}

		[XmlArrayItem ("Blog")]
		public ArrayList Blogs {
			get { return this.blogs; }
			set { this.blogs = value; }
		}

		public void Initialize (UserToken user) {
			this.user = user;

			blogs.Add (new BlogTest (this));
		}

		public void Refresh (UserToken user) {
		}

		public void Shutdown () {
		}

		public DateTime LastSaved {
			get { return last_saved; }
			set { last_saved = value; }
		}

		public event EventHandler Changed;

		internal void EmitChanged (object sender, EventArgs args) {
			if (Changed != null)
				Changed (this, args);
		}
	}
}
