
using System;
using System.Collections;
using System.Xml.Serialization;

namespace Daybook {
	public class MenuItem {
		public string              Name;
		public bool                Selected;
		public string              Group;
		internal Gtk.CheckMenuItem Item;
	}

	public class PreferencesHelper {
		// FIXME: Hack because Mono segfaults if the XmlArrayItem below
		// is uncommmented.
		public MenuItem MenuItemHack = null;

		//[XmlArrayItem ("MenuItem", Type = typeof (Daybook.MenuItem))]
		[XmlArrayItem ("MenuItem")]
		public ArrayList MenuItems = new ArrayList ();

		internal Hashtable Groups = new Hashtable ();

		public Gtk.CheckMenuItem AddCheckMenuItem (string name, bool selected) {
			return (Gtk.CheckMenuItem) AddMenuItem (name, selected, null);
		}

		public Gtk.RadioMenuItem AddRadioMenuItem (string name, bool selected, string group_name) {
			return (Gtk.RadioMenuItem) AddMenuItem (name, selected, group_name);
		}

		Gtk.MenuItem AddMenuItem (string name, bool selected, string group) {
			MenuItem item = null;

			foreach (MenuItem found in MenuItems) {
				if (found.Name == name && found.Group == group) {
					item = found;
				}
			}

			if (item == null) {
				// create a new MenuItem
				item = new MenuItem ();
				item.Name = name;
				item.Selected = selected;
				item.Group = group;

				MenuItems.Add (item);
				Changed (this, EventArgs.Empty);
			}

			if (item.Item == null) {
				if (group == null) {
					// create a check menu item
					item.Item = new Gtk.CheckMenuItem (item.Name);
				} else {
					// create a radio menu item
					if (Groups [group] == null)
						Groups [group] = new GLib.SList (IntPtr.Zero);

					item.Item = new Gtk.RadioMenuItem ((GLib.SList) Groups [group], item.Name);
					Groups [group] = ((Gtk.RadioMenuItem) item.Item).Group;
				}

				item.Item.Active = item.Selected;
				item.Item.Toggled += new EventHandler (CheckToggled);
				item.Item.Data ["storage_value"] = item;
			}

			return item.Item;
		}

		void CheckToggled (object sender, EventArgs args) {
			Gtk.CheckMenuItem check = (Gtk.CheckMenuItem) sender;

			MenuItem item = (MenuItem) check.Data ["storage_value"];
			item.Selected = check.Active;

			Changed (this, EventArgs.Empty);
		}

		public event EventHandler Changed;
	}
}
