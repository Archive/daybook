
using System;
using System.IO;
using System.Collections;
using System.Xml.Serialization;

using Gtk;

namespace Daybook {
	public interface Entry {
		void Post (bool publish);

		void Delete ();

		string Permalink {
			get;
			set;
		}

		DateTime PostDate {
			get;
			set;
		}

		string Subject {
			get;
			set;
		}

		string Content {
			get;
			set;
		}

		EntryGui DisplayWidget {
			get;
		}

		ArrayList Comments {
			get;
			set;
		}

		ArrayList Trackbacks {
			get;
			set;
		}

		[XmlIgnore]
		Blog Blog {
			get;
			set;
		}
	}

	public class TemplateDesc {
		public TemplateDesc () {
		}
	}

	public delegate void InsertHandler (object sender, Entry entry);

	public class InsertItem {
		string name;

		public string Name {
			get { return name; }
		}

		public event InsertHandler Insert;

		public InsertItem (string name, InsertHandler handler) {
			this.name = name;
			Insert += handler;
		}
	}

	public interface Blog {
		string Name {
			get;
			set;
		}

		string Url {
			get;
			set;
		}

		bool SupportsComments {
			get;
		}
		bool SupportsTrackbacks {
			get;
		}
		bool SupportsUploads {
			get;
		}

		Stream GetResource (string url);

		Gdk.Pixbuf Icon {
			get;
		}

		Entry CreateEntry ();

		ArrayList Entries {
			get;
		}

		TemplateDesc Template {
			get;
			set;
		}

		void Activate (Gui gui);
		void Deactivate (Gui gui);

		[XmlIgnore]
		Backend Backend {
			get;
			set;
		}
	}

	public interface Backend {
		string Name {
			get;
		}

		string Description {
			get;
		}

		Gdk.Pixbuf Icon {
			get;
		}

		Gtk.Widget ConfigWidget {
			get;
		}

		Gtk.Widget EditWidget {
			get;
		}

		bool CheckSettings ();

		UserToken User {
			get;
			set;
		}

		ArrayList Blogs {
			get;
			set;
		}

		Type[] GetSerializationTypes ();

		// called on activation, in its own thread
		void Initialize (UserToken user);
		void Refresh    (UserToken user);
		void Shutdown   ();

		DateTime LastSaved {
			get;
			set;
		}

		event EventHandler Changed;
	}
}
