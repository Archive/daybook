
using System;
using System.IO;
using System.Collections;
using System.Reflection;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Daybook {
	[XmlRoot ("DaybookArchive")]
	public class Archive {
		public string BackendType;

		[XmlElement ("Backend")]
		public object Backend;

		// Called by XmlSerializer
		public Archive () {
		}

		public Archive (Backend backend) {
			Type type = backend.GetType ();
			this.BackendType = type.AssemblyQualifiedName;
			this.Backend = backend;
		}

		public static Archive Read (string filename) {
			Stream file = File.OpenRead (filename);

			XmlDocument doc = new XmlDocument();
			doc.Load (file);

			XmlNode node = doc.SelectSingleNode ("//BackendType");
			string typename = node.InnerText;

			Type type = Type.GetType (typename);
			Backend backend = (Backend) Activator.CreateInstance (type);
			XmlSerializer serial = new XmlSerializer (typeof (Archive), 
								  backend.GetSerializationTypes ());

			file.Position = 0;

			Archive archive = (Archive) serial.Deserialize (file);
			archive.FixupReferences ();

			return archive;
		}

		public void Save (string filename) {
			((Backend) this.Backend).LastSaved = DateTime.Now;

			Type[] ser_types = ((Backend) this.Backend).GetSerializationTypes();
			XmlSerializer serial = new XmlSerializer (typeof (Archive), ser_types);

			Stream outstream = File.Create (filename);
			serial.Serialize (outstream, this);
			outstream.Close ();
		}

		void FixupReferences () {
			foreach (Blog blog in ((Backend) this.Backend).Blogs) {
				blog.Backend = (Backend) this.Backend;
				if (blog.Entries != null) {
					foreach (Entry entry in blog.Entries) {
						entry.Blog = blog;
					}
				}
			}
		}
	}

	internal class ConfigManager {
		static string ConfigDir = Path.Combine (Environment.GetEnvironmentVariable ("HOME"), 
							".daybook");

		//
		// Deserialize blogs stored in ~/.daybook/*.xml
		//
		internal static ArrayList ReadConfigs () {
			ArrayList loaded_backends = new ArrayList ();

			if (!Directory.Exists (ConfigDir))
				return loaded_backends;

			string[] files = Directory.GetFiles (ConfigDir, "*.xml");
			foreach (string file in files) {
				try {
					Archive archive = Archive.Read (file);
					loaded_backends.Add (archive.Backend);
				} catch (Exception e) {
					Console.WriteLine ("ERROR: Unable to find Backend type " +
							   "to load weblog data stored in file '{0}'.",
							   file);
				}
			}

			return loaded_backends;
		}

		//
		// Save weblog archive to ~/.daybook/<weblog_user>:<backend_typename>.xml
		//
		internal static void SaveConfig (Backend backend) {
			string filename = GetArchiveFileName (backend);

			EnsureConfigDir ();

			Archive archive = new Archive (backend);
			archive.Save (filename);
		}

		internal static void SaveConfigs (ArrayList backends) {
			foreach (Backend backend in backends) {
				SaveConfig (backend);
			}
		}

		//
		// Delete archive file
		//
		internal static void DeleteConfig (Backend backend) {
			string filename = GetArchiveFileName (backend);

			if (File.Exists (filename))
				File.Delete (filename);
		}

		// 
		// Delayed Backend::Changed saving 
		//
		static TimeSpan SAVE_INTERVAL = TimeSpan.FromSeconds (30);

		internal static void BackendChanged (object sender, EventArgs args) {
			Backend backend = (Backend) sender;

			if (backend.LastSaved < DateTime.Now) {
				Console.WriteLine ("!!! Queueing Serialize");
				new Timer (new TimerCallback (RunQueuedSaveConfig), 
					   backend, 
					   SAVE_INTERVAL, 
					   new TimeSpan (Timeout.Infinite));
				backend.LastSaved = DateTime.Now.Add (SAVE_INTERVAL);
			}
		}

		static void RunQueuedSaveConfig (object state) {
			Backend backend = (Backend) state;
			Console.WriteLine ("!!! Running Queued Serialize");
			SaveConfig (backend);
		}

		static void EnsureConfigDir () {
			if (!Directory.Exists (ConfigDir)) {
				DirectoryInfo d = new DirectoryInfo (ConfigDir);
				d.Create ();
			}
		}

	        static string GetArchiveFileName (Backend backend) {
			Type type = backend.GetType ();
			string filename = backend.User.Username + ":" + type.FullName + ".xml";
			return Path.Combine (ConfigDir, filename);
		}
	}

	public class BackendManager {
		static string BackendsDir = ".";

		ArrayList loaded_backends;
		ArrayList backend_types;

		public BackendManager () {
			this.backend_types = LoadBackendTypes ();
			LoadBackends (this.backend_types);
		}

		static void FindBackendsInAssembly (Assembly asm, ArrayList backend_types) {
			Type [] types = asm.GetTypes ();
			bool found_one = false;

			foreach (Type type in types) {
				foreach (Type iface in type.GetInterfaces ()) {
					if (iface == typeof (Backend)) {
						Console.Write ("{0}. ", type.FullName);
						backend_types.Add (type);
						found_one = true;
					}
				}
			}

			Console.WriteLine ("{0}", found_one ? "Done." : "Skipping.");
		}

		static ArrayList LoadBackendTypes () {
			ArrayList backend_types = new ArrayList ();

			Console.Write ("Loading static Backends... ");
			FindBackendsInAssembly (Assembly.GetCallingAssembly (), backend_types);

			string[] files = Directory.GetFiles (BackendsDir, "*.dll");
			foreach (string file in files) {
				Console.Write ("Trying Backend DLL: {0}/{1} ... ", BackendsDir, file);
				try {
					Assembly asm = Assembly.LoadFrom (file);
					FindBackendsInAssembly (asm, backend_types);
				} catch (Exception e) {
					Console.WriteLine ("Failed.\n{0}", e);
				}
			}

			return backend_types;
		}

		void LoadBackends (ArrayList backend_types) {
			loaded_backends = ConfigManager.ReadConfigs ();
			Console.WriteLine ("Reading {0} weblogs", loaded_backends.Count);

			foreach (Backend backend in loaded_backends) {
				RefreshBackend (backend);
			}

			if (loaded_backends.Count == 0) {
				// FIXME: load some dummy backends for now
				Console.Write ("Loading dummy backends...");
				foreach (Type backend_type in backend_types) {
					Backend fake_backend = CreateBackend (backend_type);
					AddBackend (fake_backend);
				}
			}
		}

		delegate void BackendInitialize (UserToken user);
		delegate void BackendRefresh (UserToken user);
		delegate void BackendShutdown ();

		void BackendInitializeDone (IAsyncResult ar) {
			Backend backend = (Backend) ar.AsyncState;

			ConfigManager.SaveConfig (backend);
			BlogsChanged (this, EventArgs.Empty);

			backend.Changed += new EventHandler (ConfigManager.BackendChanged);
		}

		void BackendRefreshDone (IAsyncResult ar) {
			BackendInitializeDone (ar);
		}

		void BackendShutdownDone (IAsyncResult ar) {
			Backend backend = (Backend) ar.AsyncState;

			ConfigManager.DeleteConfig (backend);
			BlogsChanged (this, EventArgs.Empty);
		}

		public static Backend CreateBackend (Type backend_type) {
			return (Backend) Activator.CreateInstance (backend_type);
		}

		public void AddBackend (Backend backend) {
			if (!loaded_backends.Contains (backend)) {
				loaded_backends.Add (backend);

				// Initialize Async
				BackendInitialize init = new BackendInitialize (backend.Initialize);
				init.BeginInvoke (backend.User, 
						  new AsyncCallback (BackendInitializeDone),
						  backend);
			}
		}

		public void RefreshBackend (Backend backend) {
			// Refresh Async
			BackendRefresh refresh = new BackendRefresh (backend.Refresh);
			refresh.BeginInvoke (backend.User, 
					     new AsyncCallback (BackendRefreshDone),
					     backend);
		}

		public void DeleteBackend (Backend backend) {
			if (loaded_backends.Contains (backend)) {
				loaded_backends.Remove (backend);

				// Shutdown Async
				BackendShutdown kill = new BackendShutdown (backend.Shutdown);
				kill.BeginInvoke (new AsyncCallback (BackendShutdownDone),
						  backend);
			}
		}

		public event EventHandler BlogsChanged;

		public IList Blogs {
			get {
				ArrayList ret = new ArrayList ();
				if (loaded_backends != null) {
					foreach (Backend backend in loaded_backends) {
						foreach (Blog blog in backend.Blogs) {
							ret.Add (blog);
						}
					}
				}
				return ret;
			}
		}

		public IList LoadedBackends {
			get { return loaded_backends; }
		}

		public IList BackendTypes {
			get { return backend_types; }
		}
	}
}
