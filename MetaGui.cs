
using System;
using System.Collections;

// extend gtk.table?

namespace Daybook {
	public class MetaGui : Gtk.Table {
		ArrayList Elements = new ArrayList ();

		public class MetaElem {
			MetaGui mgr;
			uint rownum;
			string name;
			Gtk.CheckMenuItem check;

			Gtk.Widget content;
			Gtk.EventBox content_ev;

			Gtk.Label name_label;
			Gtk.EventBox name_label_ev;

			public MetaElem (MetaGui mgr, 
					 uint rownum,
					 string name, 
					 Gtk.CheckMenuItem check, 
					 Gtk.Widget content) {
				this.mgr = mgr;
				this.rownum = rownum;
				this.name = name;
				this.check = check;
				this.content = content;

				name_label = new Gtk.Label (name);
				name_label.Xalign = 0.0F;
				name_label_ev = new Foregrounder (name_label);
				name_label_ev.BorderWidth = 4;
				name_label_ev = new Foregrounder (name_label_ev);

				content_ev = new Foregrounder (content);

				if (check != null)
					check.Toggled += new EventHandler (ToggledHandler);

				if (check == null || check.Active) 
					Show ();
			}

			void Show () {
				uint idx = (uint) mgr.Elements.IndexOf (this);

				mgr.Attach (name_label_ev, 
					    0, 1, 
					    rownum, rownum+1, 
					    Gtk.AttachOptions.Fill, 
					    Gtk.AttachOptions.Fill, 
					    0, 1);
				mgr.Attach (content_ev, 
					    1, 2, 
					    rownum, rownum+1, 
					    Gtk.AttachOptions.Fill | Gtk.AttachOptions.Expand,
					    Gtk.AttachOptions.Fill,
					    0, 1);
			}

			void Hide () {
				mgr.Remove (name_label_ev);
				mgr.Remove (content_ev);
			}

			void ToggledHandler (object sender, EventArgs a) {
				//Console.WriteLine ("!!!!!! MetaGui.Toggle on CheckMenuItem {0} !!!!!", name);

				if (check.Active) 
					Show ();
				else 
					Hide ();
			}
		}

		public MetaGui () : base (0, 0, false) {
			this.ColumnSpacing = 2;
			//this.RowSpacing = 1;
		}

		public void AddMetaElement (string name,
					    Gtk.CheckMenuItem check,
					    Gtk.Widget content) {
			Elements.Add (new MetaElem (this, (uint) Elements.Count, name, check, content));
		}
	}
}

/*
class MetaGui : Gtk.VBox {
	ArrayList elements = new ArrayList ();

	Gtk.VBox meta_shown_list;
	Gtk.HBox meta_list;
			
	class MetaElem {
		MetaManager mgr;
		string name;
		bool shown;
		Gtk.Widget content;
		Gtk.CheckButton check;
		Gtk.EventBox check_ev;
		Gtk.Box show_box;

		public MetaElem (MetaManager mgr, string name, bool shown, Gtk.Widget content) {
			this.name = name;
			this.shown = shown;
			this.content = content;
			this.mgr = mgr;

			check = new Gtk.CheckButton (name);
			check_ev = new Foregrounder (check);

			check.Active = shown;
			check.Toggled += new EventHandler (Toggled);
			Toggled (null, null);
		}

		void Toggled (object sender, EventArgs a) {
			if (check.Active) {
				mgr.meta_list.Remove (check_ev);

				if (show_box == null) {
					show_box = new Gtk.HBox (false, 2);
					show_box.PackStart (check_ev, false, false, 0);
					show_box.PackEnd (new Foregrounder (content));
					show_box.ShowAll ();
				} else {
					show_box.PackStart (check_ev, false, false, 0);
				}

				mgr.meta_shown_list.Add (show_box);
			} else {
				if (show_box != null) {
					mgr.meta_shown_list.Remove (show_box);
					show_box.Remove (check_ev);
				}

				mgr.meta_list.PackStart (check_ev, false, false, 0);
			}
		}
	}

	public MetaManager () : base (false, 2) {
		meta_shown_list = new Gtk.VBox (false, 2);
		meta_shown_list.Removed += new GtkSharp.RemovedHandler (RemoveCheck);
		meta_shown_list.Added += new GtkSharp.AddedHandler (AddedCheck);

		meta_list = new Gtk.HBox (false, 2);
		meta_list.Removed += new GtkSharp.RemovedHandler (RemoveCheck);
		meta_list.Added += new GtkSharp.AddedHandler (AddedCheck);
	}

	public void AddMetaElement (string name,
				    bool shown,
				    Gtk.Widget content) {
		elements.Add (new MetaElem (this, name, shown, content));
	}

	void RemoveCheck (object sender, GtkSharp.RemovedArgs args) {
		Gtk.Container parent = sender as Gtk.Container;
		Console.WriteLine ("!!! RemoveCheck Count: {0}", parent.Children.Count);
		if (parent.Children.Count == 0) {
			this.Remove (parent);
		}
	}

	void AddedCheck (object sender, GtkSharp.AddedArgs args) {
		Gtk.Container parent = sender as Gtk.Container;
		Console.WriteLine ("!!! AddedCheck Count: {0}", parent.Children.Count);
		if (parent.Children.Count == 0) {
			this.PackStart (parent, false, false, 0);
		}
	}
}
*/
