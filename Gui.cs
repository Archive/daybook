
using System;
using System.Reflection;
using System.Collections;
using System.Threading;

using Gtk;
using GtkSharp;

namespace Daybook {
	public interface Connectable {
		void Connect (GladeHelper ui);
		void Disconnect (GladeHelper ui);
	}

	public class Gui {		
		[Glade.Widget] public Gtk.Window     MainWindow;
		[Glade.Widget] public Gtk.VBox       ContentVBox;
		[Glade.Widget] public Gtk.OptionMenu BlogSelect;
		[Glade.Widget] public Gtk.MenuBar    Menubar;
		[Glade.Widget] public Gtk.Toolbar    Editbar;
		[Glade.Widget] public Gtk.MenuItem   FileMenu;
		[Glade.Widget] public Gtk.MenuItem   ViewMenu;
		[Glade.Widget] public Gtk.MenuItem   FormatMenu;
		[Glade.Widget] public Gtk.MenuItem   InsertMenu;
		[Glade.Widget] public Gtk.Combo      FindCombo;

		BackendManager backend_mgr;
		Blog active_blog;
		GladeHelper ui;

		bool content_changed = false;
		Gtk.EventBox add_weblog_box;

		//
		// About Dialog
		//

		class AboutDialog {
			[Glade.Widget] Gtk.Window AboutBox;
			[Glade.Widget] Gtk.Image LogoImage;

			AboutDialog (Gtk.Window parent) {
				GladeHelper ui = new GladeHelper ("Daybook.glade", "AboutBox");
				ui.Autoconnect (this);

				AboutBox.TransientFor = parent;
				LogoImage.Pixbuf = new Gdk.Pixbuf (null, "daybook.png");
			}

			void OnOkClicked (object sender, EventArgs a) {
				AboutBox.Hide ();
			}

			static AboutDialog instance;

			public static void Show (Gtk.Window parent) {
				if (instance == null)
					instance = new AboutDialog (parent);
				instance.AboutBox.Show ();
			}
		}

		//
		// Add Weblog Dialog
		//

		class AddWeblogDialog {
			[Glade.Widget] Gtk.Dialog AddWeblog;
			[Glade.Widget] Gtk.OptionMenu ProtocolSelect;
			[Glade.Widget] Gtk.Entry HomepageEntry;
			[Glade.Widget] Gtk.Entry UsernameEntry;
			[Glade.Widget] Gtk.Entry PasswordEntry;
			[Glade.Widget] Gtk.CheckButton SavePasswordCheck;
			[Glade.Widget] Gtk.Label WeblogOptionsLabel;
			[Glade.Widget] Gtk.EventBox ContentBox;
			[Glade.Widget] Gtk.Widget ContentBoxParent;

			Backend current_backend;
			BackendManager backend_manager;

			public AddWeblogDialog (Gtk.Window parent, 
						BackendManager backend_manager) {
				this.backend_manager = backend_manager;

				GladeHelper ui = new GladeHelper ("Daybook.glade", "AddWeblog");
				ui.Autoconnect (this);

				AddWeblog.TransientFor = parent;

				// set up the backend dropdown
				Gtk.Menu backends_menu = new Menu ();

				foreach (Type backend_type in backend_manager.BackendTypes) {
					Backend backend = BackendManager.CreateBackend (backend_type);
					HBox hbox = new HBox (false, 3);

					Gtk.Image image;
					if (backend.Icon != null)
						image = new Gtk.Image (backend.Icon);
					else 
						image = new Gtk.Image (Gui.GetDefaultMiniIcon ());
					hbox.PackStart (image, false, false, 0);

					Gtk.Label label = new Gtk.Label (backend.Name);
					label.Xalign = (float) 0.0;
					hbox.PackStart (label, true, true, 0);

					Gtk.MenuItem backend_item = new Gtk.MenuItem ();
					backend_item.Data ["backend"] = backend;
					backend_item.Add (hbox);

					backends_menu.Append (backend_item);
				}

				ProtocolSelect.Menu = backends_menu;
				ProtocolSelect.ShowAll ();

				OnProtocolChanged (null, null);
			}

			void OnCancelClicked (object sender, EventArgs a) {
				AddWeblog.Hide ();
				AddWeblog.Destroy ();
			}

			void OnOkClicked (object sender, EventArgs a) {
				if (current_backend.CheckSettings ()) {
					UserToken user = new UserToken ();

					user.Username     = UsernameEntry.Text;
					user.Password     = PasswordEntry.Text;
					user.Address      = HomepageEntry.Text;
					user.SavePassword = SavePasswordCheck.Active;

					current_backend.User = user;
					backend_manager.AddBackend (current_backend);

					OnCancelClicked (null, null);
				}
			}

			void OnProtocolChanged (object sender, EventArgs a) {
				Backend backend = (Backend) ((Gtk.Menu) ProtocolSelect.Menu).Active.Data ["backend"];
				if (backend == null)
					return;

				if (ContentBox.Child != null)
					ContentBox.Remove (ContentBox.Child);

				Gtk.Widget config = backend.ConfigWidget;
				if (config != null) {
					ContentBox.Add (config);
					WeblogOptionsLabel.Show ();
					ContentBoxParent.Show ();
				} else {
					WeblogOptionsLabel.Hide ();
					ContentBoxParent.Hide ();
				}

				this.current_backend = backend;
			}

			void OnSavePasswordToggled (object sender, EventArgs a) {
				PasswordEntry.Sensitive = SavePasswordCheck.Active;
			}
		}

		//
		// Edit Accounts Dialog
		//

		class EditAccountsDialog {
			[Glade.Widget] Gtk.Dialog EditAccounts;
			[Glade.Widget] Gtk.OptionMenu AccountSelect;
			[Glade.Widget] Gtk.Entry HomepageEntry;
			[Glade.Widget] Gtk.Entry UsernameEntry;
			[Glade.Widget] Gtk.Entry PasswordEntry;
			[Glade.Widget] Gtk.CheckButton SavePasswordCheck;
			[Glade.Widget] Gtk.Label WeblogOptionsLabel;
			[Glade.Widget] Gtk.EventBox ContentBox;
			[Glade.Widget] Gtk.Widget ContentBoxParent;

			Backend current_backend;
			Gtk.Window parent;
			BackendManager backend_manager;
			Gtk.Menu backends_menu;

			public EditAccountsDialog (Gtk.Window     parent, 
						   BackendManager backend_manager, 
						   Backend        current_backend) {
				this.parent = parent;
				this.backend_manager = backend_manager;
				this.current_backend = current_backend;

				GladeHelper ui = new GladeHelper ("Daybook.glade", "EditAccounts");
				ui.Autoconnect (this);

				EditAccounts.TransientFor = parent;

				backend_manager.BlogsChanged += new EventHandler (OnBlogsChanged);
				OnBlogsChanged (null, null);

				OnAccountChanged (null, null);
			}

			void OnCloseClicked (object sender, EventArgs a) {
				EditAccounts.Hide ();
				EditAccounts.Destroy ();
			}

			void OnNewClicked (object sender, EventArgs a) {
				new AddWeblogDialog (this.parent, this.backend_manager);
			}

			void OnDeleteClicked (object sender, EventArgs a) {
				Gtk.Dialog ask = new Gtk.MessageDialog (this.EditAccounts,
									Gtk.DialogFlags.Modal,
									Gtk.MessageType.Question,
									Gtk.ButtonsType.YesNo,
									"Really delete this Weblog?");

				if ((Gtk.ResponseType) ask.Run () == Gtk.ResponseType.Yes) {
					this.backend_manager.DeleteBackend (current_backend);

					if (this.backend_manager.LoadedBackends.Count == 0) {
						// No weblogs, show add weblog dialog
						OnNewClicked (null, null);
						OnCloseClicked (null, null);
					} else {
						backends_menu.Remove (backends_menu.Active);
						AccountSelect.SetHistory (0);
					}
				}

				ask.Hide();
			}

			void OnBlogsChanged (object sender, EventArgs args) {
				// set up the backend dropdown
				this.backends_menu = new Menu ();
				uint selected_history = 0, cnt = 0;

				foreach (Backend backend in this.backend_manager.LoadedBackends) {
					HBox hbox = new HBox (false, 3);

					Gtk.Image image;
					if (backend.Icon != null)
						image = new Gtk.Image (backend.Icon);
					else 
						image = new Gtk.Image (Gui.GetDefaultMiniIcon ());

					hbox.PackStart (image, false, false, 0);

					Gtk.Label label;
					if (backend.User.Username != null)
						label = new Gtk.Label (backend.User.Username + "@" + backend.Name);
					else
						label = new Gtk.Label (backend.Name);

					label.Xalign = (float) 0.0;
					hbox.PackStart (label, true, true, 0);

					Gtk.MenuItem backend_item = new Gtk.MenuItem ();
					backend_item.Data ["backend"] = backend;
					backend_item.Add (hbox);

					this.backends_menu.Append (backend_item);

					if (backend == this.current_backend)
						selected_history = cnt;

					cnt++;
				}

				AccountSelect.Menu = backends_menu;
				AccountSelect.SetHistory (selected_history);
				AccountSelect.ShowAll ();
			}

			void OnAccountChanged (object sender, EventArgs a) {
				Backend backend = (Backend) backends_menu.Active.Data ["backend"];
				if (backend == null)
					return;

				if (ContentBox.Child != null)
					ContentBox.Remove (ContentBox.Child);

				UsernameEntry.Text = backend.User.Username;
				PasswordEntry.Text = backend.User.Password;
				HomepageEntry.Text = backend.User.Address;
				SavePasswordCheck.Active = backend.User.SavePassword;

				Gtk.Widget edit = backend.EditWidget;
				if (edit != null) {
					ContentBox.Add (edit);
					WeblogOptionsLabel.Show ();
					ContentBoxParent.Show ();
				} else {
					WeblogOptionsLabel.Hide ();
					ContentBoxParent.Hide ();
				}

				current_backend = backend;
			}

			void OnUsernameChanged (object sender, EventArgs a) {
				current_backend.User.Username = UsernameEntry.Text;
			}

			void OnPasswordChanged (object sender, EventArgs a) {
				current_backend.User.Password = PasswordEntry.Text;
			}

			void OnSavePasswordToggled (object sender, EventArgs a) {
				PasswordEntry.Sensitive = SavePasswordCheck.Active;
				current_backend.User.SavePassword = SavePasswordCheck.Active;
			}

			void OnHomepageChanged (object sender, EventArgs a) {
				current_backend.User.Address = HomepageEntry.Text;
			}
		}

		//
		// Find Dialog
		// 

		class FindSpot {
			Entry Entry;
			int   Location;

			public void Goto () {
			}
		}

		public Queue FindSpots;

		public void FilterEntries (string search_text, bool hide_others, bool case_sensitive) {
			foreach (Entry entry in active_blog.Entries) {
				if (entry.Content.IndexOf (search_text) > -1) {
					entry.DisplayWidget.Expanded = true;
					entry.DisplayWidget.Show ();
				} else if (hide_others) {
					entry.DisplayWidget.Expanded = false;
				}
			}
		}

		public void ClearFilter () {
		}

		class FindDialog {
			[Glade.Widget] Gtk.Dialog Find;
			[Glade.Widget] Gtk.Combo FindCombo;
			[Glade.Widget] Gtk.CheckButton CaseSensitiveCheck;
			[Glade.Widget] Gtk.CheckButton ShowMatchingCheck;

			Gui gui;

			public FindDialog (Gui parent, Blog active_blog) {
				this.gui = parent;

				GladeHelper ui = new GladeHelper ("Daybook.glade", "Find");
				ui.Autoconnect (this);

				Find.TransientFor = parent.MainWindow;
			}

			void OnFindClicked (object sender, EventArgs args) {
				gui.FindCombo.Entry.Text = FindCombo.Entry.Text;
				gui.FilterEntries (FindCombo.Entry.Text, 
						   ShowMatchingCheck.Active, 
						   CaseSensitiveCheck.Active);
			}

			void OnFindEntryActivate (object sender, EventArgs args) {
				gui.FindCombo.Entry.Text = FindCombo.Entry.Text;
				gui.FilterEntries (FindCombo.Entry.Text, 
						   ShowMatchingCheck.Active, 
						   CaseSensitiveCheck.Active);
			}

			void OnCloseClicked (object sender, EventArgs args) {
				Find.Hide ();
			}
		}

		//
		// Replace Dialog
		// 

		class ReplaceDialog {
			[Glade.Widget] Gtk.Dialog Replace;
			[Glade.Widget] Gtk.Combo FindCombo;
			[Glade.Widget] Gtk.Combo ReplaceCombo;
			[Glade.Widget] Gtk.Button ReplaceButton;
			[Glade.Widget] Gtk.Button ReplaceAllButton;
			[Glade.Widget] Gtk.Button FindButton;
			[Glade.Widget] Gtk.CheckButton CaseSensitiveCheck;
			[Glade.Widget] Gtk.CheckButton UseRegexpsCheck;

			public ReplaceDialog (Gui parent, Blog active_blog) {
				GladeHelper ui = new GladeHelper ("Daybook.glade", "Replace");
				ui.Autoconnect (this);

				Replace.TransientFor = parent.MainWindow;
			}

			void OnReplaceAllClicked (object sender, EventArgs args) {
			}

			void OnReplaceClicked (object sender, EventArgs args) {
			}

			void OnFindClicked (object sender, EventArgs args) {
			}

			void OnFindEntryActivate (object sender, EventArgs args) {
				ReplaceCombo.Entry.GrabFocus ();
			}

			void OnReplaceEntryActivate (object sender, EventArgs args) {
				ReplaceButton.GrabFocus ();
			}

			void OnCloseClicked (object sender, EventArgs args) {
				Replace.Hide ();
			}
		}

		//
		// 'Save Archive' and 'Open Archive' dialogs
		//

		class ArchiveDialog {
			Gtk.FileSelection file;
			BackendManager backend_mgr;
			Blog archive_blog;

			public ArchiveDialog (BackendManager backend_mgr) {
				this.backend_mgr = backend_mgr;
			}

			public void Open () {
				file = new Gtk.FileSelection ("Open Weblog Archive");

				file.OkButton.Clicked += new EventHandler (OpenOkClicked);
				file.CancelButton.Clicked += new EventHandler (CancelClicked);

				file.ShowAll ();
			}

			void OpenOkClicked (object sender, EventArgs args) {
				foreach (string filename in file.Selections) {				
					try {
						Archive archive = Archive.Read (filename);
						Backend backend = (Backend) archive.Backend;

						// FIXME: Don't allow editing on archive blogs?

						backend_mgr.AddBackend (backend);
						new Gui (this.backend_mgr, (Blog) backend.Blogs [0]);
					} catch (Exception e) {
						// FIXME: show error about invalid archive file
						continue;
					}
				}
				file.Destroy ();
			}

			public void Save (Blog blog) {
				archive_blog = blog;

				file = new Gtk.FileSelection ("Archive Weblog");

				file.OkButton.Clicked += new EventHandler (SaveOkClicked);
				file.CancelButton.Clicked += new EventHandler (CancelClicked);

				file.ShowAll ();
			}

			void SaveOkClicked (object sender, EventArgs args) {
				foreach (string filename in file.Selections) {
					try {
						Archive archive = new Archive (archive_blog.Backend);
						archive.Save (filename);
					} catch (Exception e) {
					}
				}
				file.Destroy ();
			}

			void CancelClicked (object sender, EventArgs args) {
				file.Destroy ();
			}
		}

		//
		// Publish Changed Posts, Upload images
		//

		class Publisher {
			Blog blog;
			Gtk.HBox status_box;

			public Publisher (Blog blog, Gtk.HBox box) {
				this.blog = blog;
				this.status_box = box;
			}

			void PublishBlog () {
				foreach (Entry entry in blog.Entries) {
					//if (entry.Changed) {
					// FIXME: upload images
					//
					/*
					foreach (Image img in entry.Images) {
						if (blog.SupportsUploads) {
							blog.Backend.PutResource (img, url);
						} else {
							
						}
					}
					*/
					entry.Post (true);
					//}
				}
			}

			public void Start () {
				Thread thread = new Thread (new ThreadStart (PublishBlog));
				thread.Start ();
			}
		}

		// 
		// File menu 
		//

		void OnNewPostActivate (object sender, EventArgs a) {
			Entry new_entry = active_blog.CreateEntry ();
			if (new_entry == null)
				return;

			EntryGui new_gui = new_entry.DisplayWidget;
			new_gui.Connect (ui);
			new_gui.Expanded = true;
			new_gui.ShowAll ();

			this.ContentVBox.PackStart (new_gui, false, false, 0);
			this.ContentVBox.ReorderChild (new_gui, 0);

			new_gui.Editor.Widget.GrabFocus ();
		}

		void OnOpenActivate (object sender, EventArgs a) {
			ArchiveDialog dlg = new ArchiveDialog (this.backend_mgr);
			dlg.Open ();
		}

		void OnArchiveActivate (object sender, EventArgs a) {
			ArchiveDialog dlg = new ArchiveDialog (this.backend_mgr);
			dlg.Save (active_blog);
		}

		void OnPublishActivate (object sender, EventArgs a) {
			Publisher pub = new Publisher (active_blog, null);
			pub.Start ();
		}

		void OnRefreshActivate (object sender, EventArgs a) {
			backend_mgr.RefreshBackend (active_blog.Backend);
		}

		void OnBrowseActivate (object sender, EventArgs a) {
			if (active_blog.Url != null) 
				Gnome.Url.Show (active_blog.Url);
		}

		void OnQuitActivate (object sender, EventArgs a) {
			Gtk.Application.Quit ();
		}

		// 
		// Edit Menu
		//

		void OnFindActivate (object sender, EventArgs a) {
			FindDialog dlg = new FindDialog (this, active_blog);
		}

		void OnFindNextActivate (object sender, EventArgs a) {
			if (this.FindSpots != null) {
				FindSpot spot = (FindSpot) this.FindSpots.Dequeue ();
				if (spot != null) {
					spot.Goto ();
				}
			}
		}

		void OnReplaceActivate (object sender, EventArgs a) {
			ReplaceDialog dlg = new ReplaceDialog (this, active_blog);
		}

		void OnAccountsActivate (object sender, EventArgs a) {
			Backend current_backend = null;

			if (this.active_blog != null)
				current_backend = this.active_blog.Backend;

			EditAccountsDialog edit = new EditAccountsDialog (this.MainWindow,
									  this.backend_mgr,
									  current_backend);
		}

		// 
		// View Menu
		//

		void OnExpandAllActivate (object sender, EventArgs a) {
			foreach (Entry entry in active_blog.Entries) {
				entry.DisplayWidget.Expanded = true;
			}
		}
		void OnCollapseAllActivate (object sender, EventArgs a) {
			foreach (Entry entry in active_blog.Entries) {
				entry.DisplayWidget.Expanded = false;
			}
		}

		//
		// Help Menu
		//

		void OnAboutActivate (object sender, EventArgs a) {
			AboutDialog.Show (this.MainWindow);
		}

		//
		// Main Tool Bar
		//

		void OnFindEntryActivate (object sender, EventArgs a) {
			// search
			// FIXME
		}

		void OnFindEntryChanged (object sender, EventArgs a) {
			// set timeout before searching
			// FIXME
		}

		//
		// Blog Switching
		//

		void ActivateBlog (Blog blog) {
			if (blog == null || blog == active_blog)
				return;

			Console.WriteLine ("Activating Blog: {0}", blog);
			DeactivateBlog ();

			if (this.add_weblog_box != null) {
				this.ContentVBox.Remove (this.add_weblog_box);
				this.add_weblog_box = null;
			}

			// set the window title
			MainWindow.Title = "Daybook: " + blog.Name + " @ " + blog.Backend.Name;

			// set the window icon
			if (blog.Icon != null)
				MainWindow.Icon = blog.Icon;
			else if (blog.Backend.Icon != null)
				MainWindow.Icon = blog.Backend.Icon;
			else
				MainWindow.Icon = new Gdk.Pixbuf (null, "daybook.png");

			// let the backend populate menus
			blog.Activate (this);

			// add the blog entries
			foreach (Entry entry in blog.Entries) {
				EntryGui gui = entry.DisplayWidget;
				gui.Connect (ui);

				this.ContentVBox.PackStart (gui, false, false, 0);
			}

			this.ContentVBox.ShowAll ();
			active_blog = blog;
		}

		void DeactivateBlog () {
			if (active_blog != null) {
				active_blog.Deactivate (this);

				// clear old entries
				foreach (Entry entry in active_blog.Entries) {
					EntryGui gui = entry.DisplayWidget;
					gui.Disconnect (ui);

					this.ContentVBox.Remove (gui);
				}

				active_blog = null;
			}
		}

		void OnBlogSelectChanged (object sender, EventArgs a) {
			Gtk.RadioMenuItem menu_item = (Gtk.RadioMenuItem) sender;
			if (menu_item.Active) {
				Blog blog = (Blog) menu_item.Data ["blog"];
				if (blog != null)
					ActivateBlog (blog);
			}
		}

		static Gdk.Pixbuf GetDefaultMiniIcon () {
			Gdk.Pixbuf noicon = new Gdk.Pixbuf (null, "daybook.png");
			return noicon.ScaleSimple (16, 16, Gdk.InterpType.Nearest);
		}

		void OnAddWeblogBoxClicked (object sender, GtkSharp.ButtonPressEventArgs arg) {
			new AddWeblogDialog (this.MainWindow, this.backend_mgr);
		}

		// Add the blogs to the File menu
		void BlogsChanged (object sender, EventArgs args) {
			Gdk.Threads.Enter ();

			IList blogs = this.backend_mgr.Blogs;
			Gtk.Menu file_menu = (Gtk.Menu) this.FileMenu.Submenu;
			GLib.SList blogs_slist = new GLib.SList (IntPtr.Zero);
			int num = 1;

			// Clear old blogs
			foreach (Gtk.Widget item in file_menu.Children) {
				if (item.Data ["blog"] != null)
					file_menu.Remove (item);
			}

			if (! blogs.Contains (this.active_blog)) {
				if (blogs.Count == 0)
					DeactivateBlog ();
				else
					ActivateBlog ((Blog) blogs [0]);
			}

			switch (blogs.Count) {
			case 0:
				// No Blogs, show label with click handler to
				// show Add Weblog dialog.
				MainWindow.Icon = new Gdk.Pixbuf (null, "daybook.png");
				MainWindow.Title = "Daybook";

				ui ["BlogListSeparator"].Hide ();

				// FIXME: find a way to make this label
				// disappear if a weblog is added.

				Gtk.Label add_label = 
					new Gtk.Label ("<big><i><b>No Weblogs Configured.</b></i>\n" +
						       "<u><span color=\"blue\">Click here to add one</span></u></big>" +
						       "\n\n\n\n");
				add_label.UseMarkup = true;
				add_label.Justify = Gtk.Justification.Center;

				Gtk.EventBox box = new Gtk.EventBox();
				box.BorderWidth = 16;
				box.ButtonPressEvent += new GtkSharp.ButtonPressEventHandler (OnAddWeblogBoxClicked);
				box.Add (add_label);
				box.ShowAll ();

				this.ContentVBox.PackStart (box, false, false, 0);

				this.add_weblog_box = box;
				break;
			case 1:
				// Only show File menu check items if more than one blog
				ui ["BlogListSeparator"].Hide ();

				// FIXME: while backend is loading, show
				// label/statusbar with loading percentage.
				// Need to add Backend.Loading or
				// BackendManager.IsLoading

				break;
			default:
				ui ["BlogListSeparator"].Show ();

				// Add all the blogs to the File menu, with the
				// index number as the accelerator and the
				// blog/backend icon packed at the end
				foreach (Blog blog in blogs) {
					HBox hbox = new HBox (false, 0);

					Gtk.AccelLabel label = new Gtk.AccelLabel (String.Empty);
					label.TextWithMnemonic = String.Format ("_{0}. {1}", num++, blog.Name);
					hbox.PackStart (label, false, false, 0);

					Gtk.Image icon;
					if (blog.Icon != null)
						icon = new Gtk.Image (blog.Icon);
					else if (blog.Backend.Icon != null)
						icon = new Gtk.Image (blog.Backend.Icon);
					else
						icon = new Gtk.Image (GetDefaultMiniIcon ());
					hbox.PackEnd (icon, false, false, 0);

					Gtk.RadioMenuItem blog_item = new Gtk.RadioMenuItem (blogs_slist);
					blog_item.Data ["blog"] = blog;
					blog_item.Active = (blog == this.active_blog);
					blog_item.Add (hbox);
					blog_item.ShowAll ();
					blog_item.Activated += new EventHandler (OnBlogSelectChanged);

					label.AccelWidget = blog_item;
					blogs_slist = blog_item.Group;

					file_menu.Insert (blog_item, file_menu.Children.Count - 2);
				}
				break;
			}

			Gdk.Threads.Leave ();
		}

		Gtk.MenuItem MakeMenuItem (string name, string img_name, EventHandler handler) {
			Gtk.MenuItem mi;
			if (img_name != null) {
				Gtk.ImageMenuItem miimg = new Gtk.ImageMenuItem (name);
				miimg.Image = new Gtk.Image (new Gdk.Pixbuf (null, img_name));
				mi = miimg;
			} else {
				mi = new Gtk.MenuItem (name);
			}

			if (handler != null)
				mi.Activated += handler;

			return mi;
		}

		void SetMissingImages (GladeHelper ui) {
			Gtk.Menu insert = new Gtk.Menu ();
			insert.Append (MakeMenuItem ("_Image...", "insert-image-16.png", null));
			insert.Append (MakeMenuItem ("_Link...", "insert-link-16.png", null));
			insert.Append (MakeMenuItem ("_Table...", "insert-table-16.png", null));
			insert.Append (MakeMenuItem ("_Separator...", "insert-rule-16.png", null));
			insert.Append (MakeMenuItem ("_Text File...", null, null));
			insert.Append (MakeMenuItem ("_HTML File...", null, null));

			Gtk.MenuItem smiley = MakeMenuItem ("_Smiley", "smiley-3.png", null);
			insert.Append (smiley);

			InsertMenu.Submenu = insert;

			Gtk.Menu SmileyMenu = new Gtk.Menu ();
			SmileyMenu.Append (MakeMenuItem ("_Smile", "smiley-3.png", null));
			SmileyMenu.Append (MakeMenuItem ("_Wink", "smiley-4.png", null));
			SmileyMenu.Append (MakeMenuItem ("_Laughing", "smiley-5.png", null));
			SmileyMenu.Append (MakeMenuItem ("Oh _no!", "smiley-2.png", null));
			SmileyMenu.Append (MakeMenuItem ("_Frown", "smiley-6.png", null));
			SmileyMenu.Append (MakeMenuItem ("_Indifferent", "smiley-8.png", null));
			SmileyMenu.Append (MakeMenuItem ("_Undecided", "smiley-9.png", null));
			SmileyMenu.Append (MakeMenuItem ("Li_ck", "smiley-10.png", null));
			SmileyMenu.Append (MakeMenuItem ("Cr_ying", "smiley-11.png", null));

			smiley.Submenu = SmileyMenu;
			insert.ShowAll ();
		}

		public Gui (BackendManager backend_mgr,
			    Blog           active_blog) {
			this.backend_mgr = backend_mgr;

			ui = new GladeHelper ("Daybook.glade", "MainWindow");
			ui.ConnectWidgetTree (this);

			if (active_blog != null)
				ActivateBlog (active_blog);
			else if (backend_mgr.Blogs.Count > 0)
				ActivateBlog ((Blog) backend_mgr.Blogs [0]);

			backend_mgr.BlogsChanged += new EventHandler (BlogsChanged);
			BlogsChanged (backend_mgr, null);

			/*
			Editbar.Hide ();
			FormatMenu.Hide ();
			InsertMenu.Hide ();
			*/
			
			ui ["UndoMenuItem"].Sensitive = false;
			ui ["RedoMenuItem"].Sensitive = false;
			ui ["CutMenuItem"].Sensitive = false;
			ui ["CopyMenuItem"].Sensitive = false;
			ui ["PasteMenuItem"].Sensitive = false;
			ui ["PasteAsQuoteMenuItem"].Sensitive = false;
			ui ["ClearMenuItem"].Sensitive = false;

			SetMissingImages (ui);

			MainWindow.Show ();
		}
	}
}
